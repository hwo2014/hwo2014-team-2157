__author__ = 'pierrejeancoudert'

import json


class PJBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    def your_car(self, data):
        print("Your Car: {0}".format(data))

    def game_init(self, data):
        print("Game Init: {0}".format(data))
        self.pieces = data['race']['track']['pieces']

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switchLane(self, dir):
        self.msg("switchLane", dir)
        print "switchLane", dir

    def switchLeft(self):
        self.switchLane("Left")

    def switchRight(self):
        self.switchLane("Right")


    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def _init_car(self):
        self.pieceIndex = 0
        self.oldPieceIndex = -1
        self.oldinPieceDistance = 0
        self.inPieceDistance = 0
        self.speed = 0
        self.gameStarted = False

    def on_join(self, data):
        print("PJBot Joined")
        self._init_car()
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self._init_car()
        self.gameStarted = True
        self.ping()


    def _in_turn(self):
        return "radius" in self.pieces[self.pieceIndex]

    def _next_is_turn(self):
        next_index = (self.pieceIndex+1) % len(self.pieces)
        return "radius" in self.pieces[next_index]

    def _compute_car_parameters(self, car_position):
        self.angle = car_position[u'angle']
        self.oldPieceIndex = self.pieceIndex
        self.pieceIndex = car_position[u'piecePosition']['pieceIndex']
        self.oldinPieceDistance = self.inPieceDistance
        self.inPieceDistance = car_position[u'piecePosition']['inPieceDistance']
        if self.inPieceDistance > self.oldinPieceDistance:
          self.speed = self.inPieceDistance - self.oldinPieceDistance

    def on_car_positions(self, data):

        if not self.gameStarted:
            return

        if self.oldPieceIndex == -1:
            self.oldPieceIndex = 0
            self.switchRight()
            return

        my_car_position = data[0]
        #self.game_tick = data['gameTick']
        self._compute_car_parameters(my_car_position)
        #print self.pieces[self.pieceIndex]


        if not self._in_turn() :
            if self._next_is_turn() :
                throttle = 0.1
            else:
                throttle = 1.0
        else:
            if self._next_is_turn() :
                throttle = 0.6
            else:
                throttle = 1.0

        if throttle < 1.0:
            throttle = max(0, throttle - 0.015 * abs(self.angle))


        if self.oldPieceIndex <> self.pieceIndex:
            print self._in_turn(), 'pi', self.pieceIndex, 'd', self.inPieceDistance, 'v', self.speed, 'ang', self.angle, 'thr', throttle
        self.throttle(throttle)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.your_car,
            'gameInit': self.game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("! Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()