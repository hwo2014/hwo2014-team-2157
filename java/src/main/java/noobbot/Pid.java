package noobbot;

import java.io.FileOutputStream;
import java.nio.file.Path;

/**
 * Created by florian on 20.04.14.
 */
public class Pid {
    private  FileOutputStream out = null;
    private double kp;
    private double ki;
    private double kd;
    private int lastTick;
    private double lastError;
    private double sumI;
    private double sumD;
    private double sumP;
    Path path;

    public Pid(double kp, double ki, double kd) {
        this.kd = kd;
        this.kp = kp;
        this.ki = ki;
        lastTick  = 0;
        lastError = 0;
        sumI = 0;
        sumD = 0;
        sumP = 0;
    }

    public double doProcess(double current, double consign, int tick) {

        double value = 0;
        double error = (consign-current);
        sumP =error;
        if(lastError-error != 0) {
            sumD = (tick - lastTick) / (lastError - error);
            sumI += (tick - lastTick) * (lastError - error);
        }
        value +=  sumP * kp;
        if (ki!=0)
          value +=  sumI * ki;
        value += sumD * kd;
        lastTick = tick;
        lastError = error;
       return value;
    }
}
