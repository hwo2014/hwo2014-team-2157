package noobbot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import noobbot.messages.client.Join;
import noobbot.messages.client.Ping;
import noobbot.messages.client.SendMsg;
import noobbot.messages.server.*;

import java.io.*;
import java.net.Socket;
import java.util.Collection;

public class Main {
    private PrintWriter writer;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        GsonBuilder gb = new GsonBuilder();
        gb.registerTypeAdapter(Piece.class, new PieceDeserializer());
        Gson gson = gb.create();

        String line = null;
        CarSimulator carSimulator = new CarSimulator();
        //send(new CreateRace(join,"germany","test",1));
        //send(new JoinRace(join,"germany","test",1));
        send(join);
        while((line = reader.readLine()) != null) {
            final MsgFromServer msgFromServer = gson.fromJson(line, MsgFromServer.class);
            carSimulator.setCurrentTick(msgFromServer.gameTick);
            if (msgFromServer.msgType.equals("carPositions")) {
                Collection<CarPosition> carPositions = gson.fromJson(msgFromServer.data.toString(), new TypeToken<Collection<CarPosition>>(){}.getType());
                for (SendMsg message : carSimulator.doCallback(carPositions))
                    send(message);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init->" + msgFromServer.data.toString());
                GameInit gameInit = gson.fromJson(msgFromServer.data.toString(), GameInit.class);
                carSimulator.setGameInit(gameInit);
            } else if (msgFromServer.msgType.endsWith("turboAvailable")){
                TurboAvailable turboAvailable = gson.fromJson(msgFromServer.data.toString(),TurboAvailable.class);
                carSimulator.setTurbo(turboAvailable);
            } else if(msgFromServer.msgType.equals("yourCar")) {
                CarId yourCar = gson.fromJson(msgFromServer.data.toString(), CarId.class);
                carSimulator.setYourCar(yourCar);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                carSimulator.stop();
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                carSimulator.start();
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

