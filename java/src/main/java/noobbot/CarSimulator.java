    package noobbot;

    import noobbot.messages.client.*;
    import noobbot.messages.server.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by florian on 20.04.14.
 */
public class CarSimulator {
    private double DECELERATION_COEF = 0.02;
    private double F_STATIC = 0.40;

    private int currentTick;
    private Race race;
    private CarId ownCar;
    private TurboAvailable turbo = null;
    private int lastTick = 0;
    private double lastSpeed = 0;
    private CarPosition lastPosition = null;

    private Pid pid = new Pid(60,0,0);
    private List<SendMsg> messages = new ArrayList<SendMsg>();
    private int tickTurboUse = 0;
    private boolean started = false;

    public void setCurrentTick(int currentTick) {
        this.currentTick = currentTick;
    }

    public List<SendMsg> doCallback(Collection<CarPosition> carPositions){

        if(started) {
            messages = doProcess(carPositions);
        }else{
           messages = new ArrayList<>();
           messages.add(new Ping());
        }
        return messages;
    }
    public List<SendMsg> doProcess(Collection<CarPosition> carPositions) {
        messages.clear();

        CarPosition position = getMyOwnPosition(carPositions);

        double distance    = computeMyDistance(position);
        double speed       = computeSpeed(distance);
        double radius      = computeRadius( position.piecePosition.lane.startLaneIndex,
                                            position.piecePosition.pieceIndex);
        double force       = computeForceT(speed, radius);

        double next_radius = getNextRadius(position);
        double inerspeed   = computeInnerSpeed(next_radius);

        double decelerationDist = getDecelerationDistance(speed, inerspeed);
        double dist_next_danger = getNextDangerZone(position);

        double command = computeThrottle(force, decelerationDist, dist_next_danger);
        if(nearCard(carPositions, position)){
            if(position.piecePosition.lane.endLaneIndex > 0)
                if(race.track.pieces.get((position.piecePosition.pieceIndex+1)%race.track.pieces.size()).Switcher)
                    messages.add(new SwitchLane(SwitchLane.RIGHT));
            else
                if(race.track.pieces.get((position.piecePosition.pieceIndex+1)%race.track.pieces.size()).Switcher)
                    messages.add(new SwitchLane(SwitchLane.LEFT));
        }
        if( turbo != null
                && dist_next_danger > getDecelerationDistance(speed*turbo.turboFactor,inerspeed)
                && 0<getDecelerationDistance(speed*turbo.turboFactor,inerspeed)
                && (currentTick-tickTurboUse) > turbo.turboDurationTicks){
            messages.add(new Turbo());
            tickTurboUse = currentTick;
        }

       /* Log.log( "speed;" + speed
                + "; force ;" + force
                + ";lane;" + position.piecePosition.lane.startLaneIndex
                + ";angle;"+position.angle
                + ";Throttle;"+command
                + ";distance next danger ;"+dist_next_danger);*/

        lastTick = currentTick;
        lastSpeed = speed;
        if(lastPosition == null)
            command = 1;
        lastPosition = position;
        messages.add(new Throttle(command));
        return messages;
    }

    private boolean nearCard(Collection<CarPosition> carPositions, CarPosition position) {
        boolean result = false;

        for(CarPosition car : carPositions){
            if(!ownCar.name.equals(car.id.name)){
                double distance = computeDistanceBetweenTwoPiece( position.piecePosition,
                        car.piecePosition);
                Log.log("distance -> "+distance+" "+race.track.pieces.get(car.piecePosition.pieceIndex).length*2);
                if( distance < race.track.pieces.get(car.piecePosition.pieceIndex).length*2
                    && car.piecePosition.lane.startLaneIndex
                        == position.piecePosition.lane.startLaneIndex){
                    result = true;
                }
            }
        }
        return result;
    }

    private double computeThrottle(double force, double decelerationDist, double dist_next_danger) {
        double command = 1;
        if(dist_next_danger<decelerationDist){
            command = 0;
            if(dist_next_danger == 0) {
                command = pid.doProcess(force, F_STATIC, currentTick);
                if (command > 1) command = 1;
                if (command < 0) command = 0;
            }
        }
        return command;
    }

    private double computeMyDistance(CarPosition position) {
        double distance = 0;
        if(lastPosition != null)
            distance = computeDistanceBetweenTwoPiece(lastPosition.piecePosition, position.piecePosition);
        return distance;
    }

    private double computeSpeed(double distance) {
        double speed = 0;
        if(currentTick-lastTick != 0)
            speed = distance / (currentTick - lastTick);
        return speed;
    }

    private double computeRadius(int myLaneIndex,int indexPiece) {
        double radius = race.track.pieces.get(indexPiece).radius;
        if(radius != 0)
            if(race.track.pieces.get(myLaneIndex).angle > 0 )
                radius -=  race.track.lanes.get(myLaneIndex).distanceFromCenter;
            else
                radius +=  race.track.lanes.get(myLaneIndex).distanceFromCenter;
        return radius;
    }

    private double computeInnerSpeed(double next_radius) {
        double inerspeed = 6;
        if(next_radius!=0){
            inerspeed = Math.sqrt(F_STATIC*next_radius);
        }
        return inerspeed;
    }

    private double computeForceT(double speed, double radius) {
        double force = 0;
        if(radius != 0)
            force = speed*speed/radius;
        return force;
    }

    private double getNextRadius(CarPosition position) {
        List<Piece> pieces = race.track.pieces;
        int cpt = position.piecePosition.pieceIndex;
        double radius;
        while(pieces.get(cpt%pieces.size()).radius==0){
         cpt++;
        }
        radius = pieces.get(cpt%pieces.size()).radius;
        return radius;
    }


    private double computeDistanceBetweenTwoPiece(PiecePosition old, PiecePosition current) {
        if(old == null || current == null) return 0;
        double distance = 0;
        List <Piece> pieces = race.track.pieces;
        if(old.pieceIndex == current.pieceIndex){
            distance = current.inPieceDistance - old.inPieceDistance;
        }else{
            int count;
            if(current.pieceIndex < old.pieceIndex)
                count = current.pieceIndex + race.track.pieces.size();
            else
                count = current.pieceIndex;
            if(lastPosition != null)
                distance += pieces.get(old.pieceIndex).length - lastPosition.piecePosition.inPieceDistance;
            for(int cpt = old.pieceIndex; cpt < count-1; cpt++){
              distance+=pieces.get(cpt%pieces.size()).length;
            }
            distance+= current.inPieceDistance;
        }
        return distance;
    }

    private double getNextDangerZone(CarPosition pos) {
        List<Piece> pieces = race.track.pieces;
        int cpt = pos.piecePosition.pieceIndex;
        double distance =0;
        while(pieces.get(cpt%pieces.size()).radius==0){
            distance += pieces.get(cpt%pieces.size()).length;
            cpt++;
        }
        distance-=pos.piecePosition.inPieceDistance;
        if(distance<0){
            distance=0;
        }
        return distance;
    }


    private double getDecelerationDistance(double speed, double dest) {
        double delta = speed-dest;
        return delta/ DECELERATION_COEF;
    }

    private CarPosition getMyOwnPosition(Collection<CarPosition> carPositions) {
        CarPosition ownPosition = null;
        for(CarPosition position : carPositions){
            if(position.id.name.equals(ownCar.name) && position.id.color.equals(ownCar.color)){
                ownPosition = position;
            }
        }
        return ownPosition;
    }

    public void setGameInit(GameInit gameInit) {
        race = gameInit.race;
    }

    public void setYourCar(CarId car) {
        this.ownCar = car;
    }

    public void setTurbo(TurboAvailable turbo) {
        this.turbo = turbo;
    }

    public void start() {
        started = true;
    }

    public void stop() {
        started = false;
    }
}
