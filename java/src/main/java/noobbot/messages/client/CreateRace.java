package noobbot.messages.client;

/**
 * Created by florian on 22.04.14.
 */
public class CreateRace extends SendMsg {
    public final Join botId;
    public final String trackName;
    public final String password;
    public final int carCount;

    public CreateRace(Join botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}