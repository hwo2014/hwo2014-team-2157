package noobbot.messages.client;

import com.google.gson.Gson;
import noobbot.messages.client.MsgWrapper;

/**
 * Created by fmn on 22.04.2014.
 */
public abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}
