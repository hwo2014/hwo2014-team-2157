package noobbot.messages.client;

/**
 * Created by fmn on 22.04.2014.
 */
public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}
