package noobbot.messages.client;

/**
 * Created by fmn on 22.04.2014.
 */
public class MsgWrapper {
    public final String msgType;
    public final Object data;

    public MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }
    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }

}
