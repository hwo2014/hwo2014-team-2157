package noobbot.messages.client;

/**
 * Created by fmn on 22.04.2014.
 */
public class SwitchLane extends SendMsg{
    public static String LEFT = "Left";
    public static String RIGHT = "Right";

    private final String value;

    public SwitchLane(String value){
        this.value = value;
    }

    @Override
    protected Object msgData(){
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
