package noobbot.messages.client;

/**
 * Created by fmn on 22.04.2014.
 */
public class Turbo extends SendMsg {

    private String message = "Broom pfffffffffff";

    @Override
    protected Object msgData(){
        return message;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}
