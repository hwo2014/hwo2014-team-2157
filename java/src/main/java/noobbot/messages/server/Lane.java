package noobbot.messages.server;

/**
 * Created by florian on 18.04.14.
 */
public class Lane {
    public final int startLaneIndex;
    public final int endLaneIndex;

    public Lane(int startLaneIndex, int endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }
}
