package noobbot.messages.server;

/**
 * Created by florian on 18.04.14.
 */
public class CarPosition {
        public final CarId id;
        public final float angle;
        public final PiecePosition piecePosition;

    public CarPosition(CarId id, float angle, PiecePosition piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }
}
