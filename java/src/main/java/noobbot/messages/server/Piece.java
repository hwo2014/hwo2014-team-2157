package noobbot.messages.server;


/**
 * Created by florian on 20.04.14.
 */
public class Piece{
    public final float length;
    public final boolean Switcher;
    public final float radius;
    public final float angle;

    public Piece(float length, boolean Switcher, float radius, float angle) {
        this.length = length;
        this.radius = radius;
        this.angle = angle;
        this.Switcher = Switcher;
    }

}
