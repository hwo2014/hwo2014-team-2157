package noobbot.messages.server;

/**
 * Created by florian on 18.04.14.
 */
public class Spawn {
    public final String name;
    public final String color;

    public Spawn(String name, String color) {
        this.name = name;
        this.color = color;
    }
}
