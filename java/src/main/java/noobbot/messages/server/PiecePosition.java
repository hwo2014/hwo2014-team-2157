package noobbot.messages.server;

/**
 * Created by florian on 18.04.14.
 */
public class PiecePosition {
    public final int pieceIndex;
    public final float inPieceDistance;
    public final Lane lane;
    public final int lap;

    public PiecePosition(int pieceIndex, float inPieceDistance, Lane lane, int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }
}
