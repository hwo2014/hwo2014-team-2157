package noobbot.messages.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by florian on 20.04.14.
 */
public class PieceDeserializer implements JsonDeserializer<Piece> {

    @Override
    public Piece deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        float length = 0;
        float radius = 0;
        float angle  = 0;
        boolean Switcher = false;

        JsonElement element = jsonElement.getAsJsonObject().get("length");
        if ((element != null) && element.isJsonPrimitive()){
            length = element.getAsFloat();
        }

        element = jsonElement.getAsJsonObject().get("radius");
        if((element != null) && element.isJsonPrimitive()){
            radius = element.getAsFloat();
        }

        element = jsonElement.getAsJsonObject().get("angle");
        if((element != null) && element.isJsonPrimitive()){
            angle = element.getAsFloat();
        }

        element = jsonElement.getAsJsonObject().get("switch");
        if(element != null && element.isJsonPrimitive()){
            Switcher = element.getAsBoolean();
        }

        return new Piece(length,Switcher,radius,angle);
    }
}
