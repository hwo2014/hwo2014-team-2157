package noobbot.messages.server;

/**
 * Created by florian on 20.04.14.
 */
public class Car {
    public final CarId id;
    public final Dimension dimension;

    public Car(CarId id, Dimension dimension) {
        this.id = id;
        this.dimension = dimension;
    }
}
