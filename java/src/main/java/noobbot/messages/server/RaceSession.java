package noobbot.messages.server;

/**
 * Created by florian on 20.04.14.
 */
public class RaceSession {
    public final int laps;
    public final float maxLapTimeMs;
    public final boolean quickRace;

    public RaceSession(int laps, float maxLapTimeMs, boolean quickRace) {
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.quickRace = quickRace;
    }
}
