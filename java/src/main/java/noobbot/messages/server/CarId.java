package noobbot.messages.server;

/**
 * Created by florian on 19.04.14.
 */
public class CarId {
    public final String name;
    public final String color;

    public CarId(String name, String color) {
        this.name = name;
        this.color = color;
    }
}
