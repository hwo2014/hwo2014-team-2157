package noobbot.messages.server;

import java.util.List;

/**
 * Created by florian on 20.04.14.
 */
public class Race {
    public final Track track;
    public final List<Car> cars;
    public final RaceSession raceSession;

    public Race(Track track, List<Car> cars, RaceSession raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }
}
