package noobbot.messages.server;

/**
 * Created by florian on 19.04.14.
 */
public class RaceTime {
    public final int laps;
    public final int ticks;
    public final int millis;

    public RaceTime(int laps, int ticks, int millis) {
        this.laps = laps;
        this.ticks = ticks;
        this.millis = millis;
    }
}
