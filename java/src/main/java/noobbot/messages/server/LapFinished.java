package noobbot.messages.server;

/**
 * Created by florian on 19.04.14.
 */
public class LapFinished {
    public final CarId car;
    public final LapTime lapTime;
    public final RaceTime raceTime;
    public final Ranking ranking;

    public LapFinished(CarId car, LapTime lapTime, RaceTime raceTime, Ranking ranking) {
        this.car = car;
        this.lapTime = lapTime;
        this.raceTime = raceTime;
        this.ranking = ranking;
    }
}
