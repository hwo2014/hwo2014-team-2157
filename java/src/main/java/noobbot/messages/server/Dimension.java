package noobbot.messages.server;

/**
 * Created by florian on 20.04.14.
 */
public class Dimension {
    public final float length;
    public final float width;
    public final float guideFlagPosition;

    public Dimension(float length, float width, float guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }
}
