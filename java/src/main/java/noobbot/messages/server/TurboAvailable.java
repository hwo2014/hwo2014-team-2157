package noobbot.messages.server;

import noobbot.messages.client.SendMsg;

/**
 * Created by fmn on 22.04.2014.
 */
public class TurboAvailable extends SendMsg {
    public final double turboDurationMilliseconds;
    public final int    turboDurationTicks;
    public final int    turboFactor;

    TurboAvailable(double turboDurationMilliseconds, int turboDurationTicks, int turboFactor) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
        this.turboDurationTicks = turboDurationTicks;
        this.turboFactor = turboFactor;
    }


    @Override
    protected String msgType() {
        return "turboAvailable";
    }
}
