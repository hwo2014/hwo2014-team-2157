package noobbot.messages.server;

/**
 * Created by florian on 20.04.14.
 */
public class MsgFromServer {
    public final String msgType;
    public final Object data;
    public final String gameId;
    public final int gameTick;

    public MsgFromServer(String msgType, Object data, String gameId, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }
}
