package noobbot.messages.server;

/**
 * Created by fmn on 23.04.2014.
 */
public class LaneSheet {
    public final int distanceFromCenter;
    public final int index;

    public LaneSheet(int distanceFromCenter, int index) {
        this.distanceFromCenter = distanceFromCenter;
        this.index = index;
    }
}
