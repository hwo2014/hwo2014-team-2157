package noobbot.messages.server;


import java.util.List;

/**
 * Created by florian on 20.04.14.
 */
public class Track {
    public final String id;
    public final String name;
    public final List<Piece> pieces;
    public final List<LaneSheet> lanes;
    public final List<Car>  cars;

    public Track(String id, String name, List<Piece> pieces, List<LaneSheet> lanes, List<Car> cars) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;
        this.cars = cars;
    }

}
