package noobbot.messages.server;

/**
 * Created by florian on 18.04.14.
 */
public class Crash {
    public final String name;
    public final String color;

    public Crash(String name, String color) {
        this.name = name;
        this.color = color;
    }
}
