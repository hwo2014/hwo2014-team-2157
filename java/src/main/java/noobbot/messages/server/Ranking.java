package noobbot.messages.server;

/**
 * Created by florian on 19.04.14.
 */
public class Ranking {
    public final int overall;
    public final int fastestLap;

    public Ranking(int overall, int fastestLap) {
        this.overall = overall;
        this.fastestLap = fastestLap;
    }
}
