package noobbot.messages.server;

/**
 * Created by florian on 19.04.14.
 */
public class LapTime {
    public final int lap;
    public final int ticks;
    public final int millis;

    public LapTime(int lap, int ticks, int millis) {
        this.lap = lap;
        this.ticks = ticks;
        this.millis = millis;
    }
}
